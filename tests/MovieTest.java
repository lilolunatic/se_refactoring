import org.junit.Assert;
import org.junit.Test;

public class MovieTest {

    private final Movie movie = new Movie("The Godfather", 1);

    @Test
    public void getPriceCode() {
        Assert.assertEquals(1, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(2);
        Assert.assertEquals(2,movie.getPriceCode());
    }

    @Test
    public void getTitle() {
        Assert.assertEquals("The Godfather", movie.getTitle());
    }
}