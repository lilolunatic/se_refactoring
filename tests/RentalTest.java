import org.junit.Assert;
import org.junit.Test;

public class RentalTest {

    private final Movie movie = new Movie("Some like it hot", 0);
    private final Rental rental = new Rental(movie , 9);

    @Test
    public void getDaysRented() {
        Assert.assertEquals(9, rental.getDaysRented());
    }

    @Test
    public void getMovie() {
        Assert.assertEquals(movie, rental.getMovie());
    }

    @Test
    public void charge(){
        double regularAmount = 12.5;
        Assert.assertEquals(regularAmount, rental.charge(), 0.5);
    }

    @Test
    public void renterPoints(){
        int renterPoints = 1;
        Assert.assertEquals(renterPoints, rental.renterPoints());
    }
}