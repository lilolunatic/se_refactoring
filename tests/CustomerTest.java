import org.junit.Assert;
import org.junit.Test;

public class CustomerTest {

    private final Customer customer = new Customer("Max Mustermann");
    private final Movie titanic = new Movie("Titanic", 12);
    private final Rental titanicRental = new Rental(titanic, 3);

    @Test
    public void addRental() {
        customer.addRental(titanicRental);
        Assert.assertEquals(titanicRental, customer.getRentals().elementAt(0));
    }

    @Test
    public void getName() {
        Assert.assertEquals("Max Mustermann", customer.getName());
    }
}