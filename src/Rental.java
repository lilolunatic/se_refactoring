class Rental {
    private final Movie movie;
    private final int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public double charge() {
        double rentalAmount = 0;
        switch (this.getMovie().getPriceCode()) {
            case Movie.REGULAR:
                rentalAmount += 2;
                if (this.getDaysRented() > 2)
                    rentalAmount += (this.getDaysRented() - 2) * 1.5;
                break;
            case Movie.NEW_RELEASE:
                rentalAmount += this.getDaysRented() * 3;
                break;
            case Movie.CHILDRENS:
                rentalAmount += 1.5;
                if (this.getDaysRented() > 3)
                    rentalAmount += (this.getDaysRented() - 3) * 1.5;
                break;
        }
        return rentalAmount;
    }

    public int renterPoints(){
        int frequentRenterPoints = 1;

        // add bonus for a two day new release rental
        if ((this.getMovie().getPriceCode() == Movie.NEW_RELEASE) && this.getDaysRented() > 1)
            frequentRenterPoints++;

        return frequentRenterPoints;
    }

}