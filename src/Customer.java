import java.util.Enumeration;
import java.util.Vector;

@SuppressWarnings("unchecked")
class Customer {
    private final String name;
    private final Vector rentals = new Vector();
    private int frequentRenterPoints;

    public Customer(String newName) {
        name = newName;
        frequentRenterPoints = 0;
    }

    @SuppressWarnings("unchecked")
    public void addRental(Rental arg) {
        rentals.addElement(arg);
    }

    public Vector getRentals() {
        return rentals;
    }

    public String getName() {
        return name;
    }

    public String rentalRecord() {
        Enumeration rentalElements = rentals.elements();
        StringBuilder result = new StringBuilder("Rental Record for " + this.getName() + "\n");
        result.append("\t" + "Title" + "\t" + "\t" + "Days" + "\t" + "Amount" + "\n");

        double totalAmount = 0;
        while (rentalElements.hasMoreElements()) {
            double rentalAmount;
            Rental rental = (Rental) rentalElements.nextElement();

            //determine amounts for each line
            rentalAmount = rental.charge();

            //get renter points
            this.frequentRenterPoints += rental.renterPoints();

            //show figures for this rental
            result.append("\t").append(rental.getMovie().getTitle()).append("\t").append("\t").append(rental.getDaysRented()).append("\t").append(rentalAmount).append("\n");
            totalAmount += rentalAmount;
        }
        //add footer lines
        result.append("Amount owed is ").append(totalAmount).append("\n");
        result.append("You earned ").append(this.frequentRenterPoints).append(" frequent renter points");
        return result.toString();
    }
}

    